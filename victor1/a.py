def qsort(arr): 
    if len(arr) <= 1:
        return arr
    else:
        return qsort([x for x in arr[1:] if x < arr[0]]) + [arr[0]] + qsort([x for x in arr[1:] if x >= arr[0]])

a=[1,2,3,4,5,6,7,8,9]
a=a+a

print (a)
print (qsort(a))