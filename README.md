# PythonPlay

Playground for Python

## Dev environment
If using [VS Code](https://code.visualstudio.com), no local Python environment is required. 
This project is configured to use [VS Code Remote Development](https://code.visualstudio.com/docs/remote/remote-overview) in [Docker container](https://code.visualstudio.com/docs/remote/containers#_indepth-setting-up-a-folder-to-run-in-a-container)